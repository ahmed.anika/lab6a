import java.util.Scanner;

public class LuckyCardGameApp {
    public static void main (String [] args){
         GameManager manager = new GameManager();
		 int totalPoints = 0;
         int round = 1;

        System.out.println("Welcome to Lucky Card Game!");
        
        while (manager.getNumberOfCards() > 1 && totalPoints < 5) {
            System.out.println("Round " + round);
            System.out.println(manager);
            totalPoints += manager.calculatePoints();
            System.out.println("Your points after round " + round + ": " + totalPoints);
            System.out.println("\n*******************************************************************************");
            round++;
            manager.dealCards();
        }

        if (totalPoints >= 5) {
			System.out.println("Player wins with: " + totalPoints + " points");
		} else {
			System.out.println("Player lost with: " + totalPoints + " points");
		}
	} 
    
}